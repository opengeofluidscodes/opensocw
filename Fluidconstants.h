#ifndef __FLUIDCONSTANTS_H_
#define __FLUIDCONSTANTS_H_

// Critical Properties of water are for the Haar-Gallagher-Kell (IAPS-84) formulation
const double tcrit_water   = 373.976e0;
const double tcrit_waterK  = tcrit_water+273.15e0;
const double pcrit_water   = 220.54915e0;
const double rhocrit_water = 321.89e0;
const double hcrit_water   = 2086.0e0;

// Constants in the pure NaCl side of the system
const double nacl_ttriple  = 800.7e0;
const double nacl_ptriple  = 5.0e-4;

#endif
