#include<cmath>
#include<iostream>
#include "eos_socw2000.h"

using namespace std;

Eos_socw2000::Eos_socw2000()
    :
      socw_parameters
      {
        { 4.21345e-3,  1.76721e-4, -0.69122e-5,  0.41610, -0.13768,  0.0     }, //CH4
        { 3.39309e-3,  1.48009e-4, -1.38796e-5,  0.55582, -0.26387,  0.0     }, //CO2
        { 1.99299e-3,  1.54007e-4, -1.91120e-5,  0.59693, -0.38816,  0.0     }, //H2S
        { 1.32548e-3,  0.49738e-4, -0.80535e-5,  0.88078, -0.18104,  0.0     }, //NH3
        {-0.61428e-3,  0.38857e-4,  0.86324e-5,  1.82268,  0.12002,  0.0     }, //H3BO3
        { 3.80910e-3,  2.16619e-4, -1.63335e-5,  0.58783, -0.33402,  0.0     }, //C2H4
        { 4.96236e-3,  1.30750e-4,  0.20640e-5,  0.33230,  0.04698,  0.0     }, //Ar
        { 0.0       ,  0.0       ,  0.0       ,  0.0    ,  0.0    ,  0.0     }, //H+
        {-7.1394e-3 ,  1.84661e-4, -0.82402e-5, -1.27172,  0.97971e2, -0.51155 }, //Li+
        {-7.1136e-3 ,  2.31260e-4, -3.02894e-5, -1.52032,  2.94922e2, -1.14565 }, //Na+
        {-7.5291e-3 ,  2.18516e-4, -2.57981e-5, -0.80917,  2.49693e2, -0.79125 }, //K+
        {-5.8022e-3 ,  1.90946e-4, -2.55954e-5,  0.06822,  2.52611e2, -0.69451 }, //Cs+
        {-2.7026e-3 ,  1.08730e-4, -0.95674e-5,  0.28730,  0.99682e2, -0.36821 }, //NH4+
        {-0.656755  , -0.86168e-4, -0.30967e-5,  2.56146,  0.16432e2,  0.26063 }, //Cl-
        {-0.654326  , -1.20912e-4, -0.84933e-5,  3.19590,  0.64270e2,  0.11044 }, //Br-
        {-0.650966  , -1.56554e-4, -1.43758e-5,  4.05431,  1.04893e2,  0.09678 }, //I-
        {-0.661106  ,  0.501544-4, -1.88666e-5,  0.40718,  1.88351e2, -0.59552 }  //OH-
      },
      socw_speciesnames{ "CH4", "CO2", "H2S", "NH3", "H3BO3", "C2H4", "Ar", "H+", "Li+", "Na+", "K+", "Cs+", "NH4+", "Cl-", "Br-", "I-", "OH-" }
{

}

double Eos_socw2000::PartialMolarVolume(const double& tK,    const double& rho,
                                        const double& kappa, const socw_species& species)
{
    const double R  = 8.314;
    const unsigned long i     = static_cast<unsigned long>(species);
    const double v0 = 0.018015/rho;
    double v2       = kappa*R*tK;
    const double a  = socw_parameters[i][0];
    const double b  = socw_parameters[i][1];
    const double c  = socw_parameters[i][2];
    const double d  = socw_parameters[i][3];
    double delta{0.};
    if(i <= static_cast<unsigned long>(socw_species::Ar)) delta = 0.35*a;
    else if (i <= static_cast<unsigned long>(socw_species::NH4_plus)) delta = 0.0;
    else delta = -0.645;
    const double lambda = -0.01;
//    cout << "kappa                       = " << kappa << endl;
//    cout << "k0 R T                      = " << kappa*R*tK << endl;
//    cout << "d * (v0-kappa*R*tK)         = " << d * (v0-kappa*R*tK) << endl;
//    cout << "1-d-a*rho                   = " << 1-d-a*rho << endl;
//    cout << "c*exp(1500./tK              = " << c*exp(1500./tK) << endl;
//    cout << "b*(exp(0.005*rho)-1.0)      = " << b*(exp(0.005*rho)-1.0) << endl;
//    cout << "delta*(exp(lambda*rho)-1.0) = " << delta*(exp(lambda*rho)-1.0) << endl;
////    cout << "sum                       = " << (a + c*exp(1500./tK) + b*(exp(0.005*rho)-1.0) + delta*(exp(lambda*rho)-1.0))*kappa*R*tK*rho << endl;
//    cout << "a0 = " << d*v0 << endl;
//    cout << "a1 = " << 1. - d - a*rho + b*rho*(exp(0.005*rho)-1.0) + delta*rho*(exp(lambda*rho)-1.0) << endl;
//    cout << "a2 = " << c*rho << endl;
//    cout << "a2*eterm*kappaRT = " << c*rho*exp(1500./tK)*kappa*8.314*tK << endl;

    v2       += d * (v0-kappa*R*tK);
    v2       += (a + c*exp(1500./tK) + b*(exp(0.005*rho) - 1.0)+delta*(exp(lambda*rho)-1.0))*kappa*R*tK*rho;
    return v2;
}

double Eos_socw2000::a1Term(const double& rho,
                            const socw_species& species)
{
    const double R  = 8.314;
    const unsigned long i     = static_cast<unsigned long>(species);
    const double v0 = 0.018015/rho;
    const double a  = socw_parameters[i][0];
    const double b  = socw_parameters[i][1];
    const double d  = socw_parameters[i][3];
    double delta{0.};
    if(i <= static_cast<unsigned long>(socw_species::Ar)) delta = 0.35*a;
    else if (i <= static_cast<unsigned long>(socw_species::NH4_plus)) delta = 0.0;
    else delta = -0.645;
    const double lambda = -0.01;

    return R/v0*(1. - d - a*rho + b*rho*(exp(0.005*rho)-1.) + delta*rho*(exp(lambda*rho)-1.) );
}

double Eos_socw2000::a2Term(const double& tK,
                            const double& rho,
                            const socw_species& species)
{
    const double R  = 8.314;
    const unsigned long i     = static_cast<unsigned long>(species);
    const double v0 = 0.018015/rho;
    const double c  = socw_parameters[i][2];

    return R/v0*c*rho*exp(1500/tK);
}



double Eos_socw2000::Term1(const double &tK, const double &kappa)
{
    const double R  = 8.314;
    return kappa*R*tK*1.0e6;
}

double Eos_socw2000::Term2(const double &tK, const double &rho, const double &kappa, const socw_species &species)
{
    const double R  = 8.314;
    const unsigned long i     = static_cast<unsigned long>(species);
    const double v0 = 0.018015/rho;
    const double d  = socw_parameters[i][3];

    return d * (v0-kappa*R*tK)*1.0e6;
}

double Eos_socw2000::Term3(const double& tK,    const double& rho,
                                        const double& kappa, const socw_species& species)
{
    const double R  = 8.314;
    const unsigned long i     = static_cast<unsigned long>(species);
    const double a  = socw_parameters[i][0];
    const double b  = socw_parameters[i][1];
    const double c  = socw_parameters[i][2];
    double delta{0.};
    if(i <= static_cast<unsigned long>(socw_species::Ar)) delta = 0.35*a;
    else if (i <= static_cast<unsigned long>(socw_species::NH4_plus)) delta = 0.0;
    else delta = -0.645;
    const double lambda = -0.01;

    return (a + c*exp(1500./tK) + b*(exp(0.005*rho) - 1.0)+delta*(exp(lambda*rho)-1.0))*kappa*R*tK*rho*1.0e6;
}


double Eos_socw2000::Term3_1(const socw_species& species)
{
    const unsigned long i     = static_cast<unsigned long>(species);
    return socw_parameters[i][0];
}


double Eos_socw2000::Term3_2(const double& tK, const socw_species& species)
{
    const unsigned long i     = static_cast<unsigned long>(species);
    const double c  = socw_parameters[i][2];
    return (c*exp(1500./tK));
}

double Eos_socw2000::Term3_3(const double& rho, const socw_species& species)
{
    const unsigned long i     = static_cast<unsigned long>(species);
    const double b  = socw_parameters[i][1];
    return b*(exp(0.005*rho) - 1.0);
}

double Eos_socw2000::Term3_4(const double& rho,const socw_species& species)
{
    const unsigned long i     = static_cast<unsigned long>(species);
    const double a  = socw_parameters[i][0];
    double delta{0.};
    if(i <= static_cast<unsigned long>(socw_species::Ar)) delta = 0.35*a;
    else if (i <= static_cast<unsigned long>(socw_species::NH4_plus)) delta = 0.0;
    else delta = -0.645;
    const double lambda = -0.01;

    return delta*(exp(lambda*rho)-1.0);
}
