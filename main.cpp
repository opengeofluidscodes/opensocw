#include<iostream>
#include<fstream>
#include<cmath>
#include<string>
#include "eos_socw2000.h"
#include "socw_species.h"
#include "Fluidconstants.h"
#include "steam4.h"

using namespace std;

int main()
{
    double p{1.0};
    double tK{0.};
    double rho{1000.};
    double kappa{0.};
    double v1{0.};

    Eos_socw2000 eos;
    Prop *prost;
    Prop *satv;
    Prop *satl;
    prost = newProp('T', 'p', 1);
    satv  = newProp('T', 'p', 1);
    satl  = newProp('T', 'p', 1);


    // *** isochores: v2/kappa for all SOCW species ***
    for(int i = 0; i < eos.Number_of_SOCW_Species(); ++i)
    {
        socw_species species = static_cast<socw_species>(i);
        string filename = eos.SOCW_species(i) + "_v2overkappa_isochores.dat";
        ofstream ofile(filename);
        for(rho = 300.; rho < 1001.; rho += 100.)
        {
            v1 = 0.018015/rho;
            ofile << "#" << rho << "kg/m3\n";
            for(tK = 298.15; tK < 874.14; tK += 5.)
            {
                if(tK < tcrit_waterK)
                {
                    sat_t(tK,satl,satv);
                    if(rho < satl->d + 1.) continue;
                }
                water_td(tK,rho,prost);
                p = prost->p;
                water_td(tK,rho+1.0e-1,prost);
                kappa = 1.0e-1/rho/( prost->p - p );
                double v2 = eos.PartialMolarVolume(tK,rho,kappa,species);
                double krichevskii = -(1.-rho/0.018015*v2)/kappa;
                ofile << tK << "\t" << v2/kappa << "\t" << v1/kappa << "\t" << v1*krichevskii << endl;
            }
            ofile << endl;
        }
        ofile.close();
    }


//    // *** isochores: krichevski/T vs. 1/(T kappa)
//    for(int i = 0; i < eos.Number_of_SOCW_Species(); ++i)
//    {
//        socw_species species = static_cast<socw_species>(i);
//        string filename = eos.SOCW_species(i) + "_krichevskii_isochores.dat";
//        ofstream ofile(filename);
//        for(rho = 300.; rho < 1001.; rho += 100.)
//        {
//            v1 = 0.018015/rho;
//            ofile << "#" << rho << "kg/m3\n";
//            for(tK = 298.15; tK < 874.14; tK += 5.)
//            {
//                if(tK < tcrit_waterK)
//                {
//                    sat_t(tK,satl,satv);
//                    if(rho < satl->d + 1.) continue;
//                }
//                water_td(tK,rho,prost);
//                p = prost->p;
//                water_td(tK,rho+1.0e-1,prost);
//                kappa = 1.0e-1/rho/( prost->p - p );
//                double v2 = eos.PartialMolarVolume(tK,rho,kappa,species);
//                double krichevskii = -(1.-rho/0.018015*v2)/kappa;
//                ofile << 1./(kappa*tK) << "\t" << krichevskii/tK << endl;
//            }
//            ofile << endl;
//        }
//        ofile.close();
//    }


    freeProp(prost);
    freeProp(satv);
    freeProp(satl);


}
