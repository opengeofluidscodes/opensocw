#ifndef SOCW_SPECIES_H
#define SOCW_SPECIES_H

enum socw_species{ CH4, CO2, H2S, NH3, H3BO3, C2H4, Ar, H_plus, Li_plus, Na_plus, K_plus, Cs_plus, NH4_plus, Cl_minus, Br_minus, I_minus, OH_minus, number_of_socw_species };

#endif // SOCW_SPECIES_H
