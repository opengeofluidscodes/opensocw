#ifndef EOS_SOCW2000_H
#define EOS_SOCW2000_H

#include <vector>
#include <string>
#include "socw_species.h"

class Eos_socw2000
{
public:
    Eos_socw2000();
    int Number_of_SOCW_Species();
    std::string SOCW_species(int i);
    double PartialMolarVolume(const double& tK,
                              const double& rho,
                              const double& kappa,
                              const socw_species& species);
    double a1Term(const double& rho,
                  const socw_species& species);
    double a2Term(const double& tK,
                  const double& rho,
                  const socw_species& species);
    double Term1(const double& tK,
                 const double& kappa);
    double Term2(const double& tK,
                 const double& rho,
                 const double& kappa,
                 const socw_species& species);
    double Term3(const double& tK,
                 const double& rho,
                 const double& kappa,
                 const socw_species& species);

    double Term3_1(const socw_species& species);
    double Term3_2(const double& tK,  const socw_species& species);
    double Term3_3(const double& rho, const socw_species& species);
    double Term3_4(const double& rho, const socw_species& species);
//    double a0;
//    double a1;
//    double a2;

private:

    const int number_of_socw_species = static_cast<int>(socw_species::number_of_socw_species);
    std::vector<std::vector<double> > socw_parameters;
    std::vector<std::string> socw_speciesnames;
};

inline int Eos_socw2000::Number_of_SOCW_Species(){return number_of_socw_species;}
inline std::string Eos_socw2000::SOCW_species(int i){return socw_speciesnames[i];}

#endif // EOS_SOCW2000_H
